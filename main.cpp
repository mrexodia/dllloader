#include <windows.h>
#include <commctrl.h>
#include "resource.h"

static HINSTANCE hInst;

static BOOL CALLBACK DlgMain(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch(uMsg)
    {
    case WM_INITDIALOG:
    {
#ifdef _WIN64 //x64
        SetWindowTextA(hwndDlg, "DLL Loader x64");
#else //x86
        SetWindowTextA(hwndDlg, "DLL Loader x86");
#endif // _WIN64
        SendMessageA(hwndDlg, WM_SETICON, ICON_BIG, (LPARAM)LoadIconA(hInst, MAKEINTRESOURCEA(IDI_ICON1)));
    }
    return TRUE;

    case WM_CLOSE:
    {
        EndDialog(hwndDlg, 0);
    }
    return TRUE;

    case WM_DROPFILES:
    {
        wchar_t szFile[MAX_PATH] = L"";
        UINT uFile = 0;
        HDROP hDrop = (HDROP)wParam;
        uFile = DragQueryFileW(hDrop, 0xFFFFFFFF, NULL, 0);
        if (uFile != 1)
        {
            MessageBoxA(hwndDlg, "Dropping multiple files is not supported.", "Error", MB_ICONERROR);
            DragFinish(hDrop);
            break;
        }
        szFile[0] = '\0';
        if(DragQueryFileW(hDrop, 0, szFile, MAX_PATH))
        {
            wchar_t szFileDirectory[MAX_PATH]=L"";
            wcscpy(szFileDirectory, szFile);
            int len = wcslen(szFileDirectory);
            while(szFileDirectory[len] != '\\' && len)
                len--;
            if(len)
                szFileDirectory[len] = L'\0';
            //set PATH
            wchar_t* szOldPath = (wchar_t*)malloc(32767 * sizeof(wchar_t));
            wchar_t* szNewPath = (wchar_t*)malloc(32767 * sizeof(wchar_t));
            GetEnvironmentVariableW(L"PATH", szOldPath, 32767);
            wcscpy(szNewPath, szOldPath);
            wcscat(szNewPath, L";");
            wcscat(szNewPath, szFileDirectory);
            SetEnvironmentVariableW(L"PATH", szNewPath);
            free(szNewPath);
            //load dll
            HMODULE hLib = LoadLibraryW(szFile);
            if(hLib)
            {
                char msg[256] = "";
                FreeLibrary(hLib);
                void* ImageBase = VirtualAlloc(hLib, 0x1000, MEM_COMMIT|MEM_RESERVE, PAGE_READWRITE); //reserve old imagebase
                hLib = LoadLibraryW(szFile);
                if(hLib)
                {
                    FreeLibrary(hLib);
                    wsprintfA(msg, "DLL Loaded on 0x%p and on 0x%p!", ImageBase, hLib);
                    MessageBoxA(hwndDlg, msg, "Unpacking OK!", MB_ICONINFORMATION);
                }
                else
                {
                    wsprintfA(msg, "DLL Loaded on 0x%p only, other load failed!", hLib);
                    MessageBoxA(hwndDlg, msg, "Relocations FAILED", MB_ICONWARNING);
                }
                VirtualFree(ImageBase, 0, MEM_DECOMMIT|MEM_RELEASE); //free old imagebase
            }
            else
                MessageBoxA(hwndDlg, "Failed to load DLL (DllMain/Invalid)!", "Unpacking FAILED", MB_ICONERROR);

            //restore path
            SetEnvironmentVariableW(L"PATH", szOldPath);
            free(szOldPath);

            DragFinish(hDrop);
        }
        return TRUE;
    }
    return FALSE;
    }
    return FALSE;
}

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
    hInst = hInstance;
    InitCommonControls();
    return DialogBox(hInst, MAKEINTRESOURCE(DLG_MAIN), NULL, (DLGPROC)DlgMain);
}
